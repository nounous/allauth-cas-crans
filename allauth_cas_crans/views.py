from allauth_cas.views import CASAdapter, CASCallbackView, CASLoginView, CASLogoutView

from .provider import CransProvider


class CransAdapter(CASAdapter):
    provider_id = CransProvider.id
    url = "https://cas.crans.org"
    version = 3


login = CASLoginView.adapter_view(CransAdapter)
callback = CASCallbackView.adapter_view(CransAdapter)
logout = CASLogoutView.adapter_view(CransAdapter)
