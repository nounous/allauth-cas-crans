from django.conf.urls import url

from . import views

urlpatterns = [
    url("^crans/login/$", views.login, name="crans_login"),
    url("^crans/callback/$", views.callback, name="crans_callback"),
    url("^crans/logout/$", views.logout, name="crans_logout"),
]
