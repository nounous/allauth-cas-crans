# Copyright (C) 2021 Crans
# SPDX-License-Identifier: GPL-3.0

"""
Django AllAuth provider for the Crans CAS.
"""

try:
    from allauth_cas_crans.version import version as __version__
except ImportError:
    __version__ = "dev"

# See https://www.python.org/dev/peps/pep-0008/#module-level-dunder-names
__all__ = [
    "__version__",
]
