from allauth.socialaccount.providers.base import ProviderAccount
from allauth_cas.providers import CASProvider


class CransAccount(ProviderAccount):
    pass


class CransProvider(CASProvider):
    id = "crans"
    name = "Compte Crans"
    account_class = CransAccount


provider_classes = [CransProvider]
