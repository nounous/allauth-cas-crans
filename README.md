# AllAuth Crans provider

Django AllAuth provider for the Crans CAS.

```bash
sudo pip install --upgrade git+https://gitlab.crans.org/nounous/allauth-crans.git
```
